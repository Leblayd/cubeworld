﻿using System;
using System.Collections.Generic;
using Cubeworld;
using NUnit;
using NUnit.Framework;

namespace Kockavilag.Test
{
    [TestFixture]
    public class TestArrange
    {
        [Test]
        public void TheAssignment()
        {
            Table table = new Table(2, 0, 0, 3);
            List<string> expected = new List<string>
            {
                "1 0",
                "2 1",
                "4 0",
                "3 2",
                "4 3"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_AlreadyCorrect()
        {
            Table table = new Table(0, 1, 2, 3, 4, 5, 6);
            List<string> expected = new List<string>();

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_SingleCube()
        {
            Table table = new Table(0);
            List<string> expected = new List<string>();

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_ReversedSingleColumn()
        {
            Table table = new Table(2, 3, 4, 5, 6, 7, 0);
            List<string> expected = new List<string>
            {
                "1 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_ReversedSingleColumn_WithCorrectEnds()
        {
            Table table = new Table(0, 3, 4, 5, 6, 1, 2);
            List<string> expected = new List<string>
            {
                "7 0",
                "2 0",
                "3 0",
                "4 0",
                "5 0",
                "6 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_JumbledSingleColumn()
        {
            Table table = new Table(6, 3, 1, 5, 7, 0, 2);
            List<string> expected = new List<string>
            {
                "4 0",
                "5 0",
                "7 0",
                "2 0",
                "3 0",
                "1 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_JumbledSingleColumn_WithCorrectEnds()
        {
            Table table = new Table(0, 3, 6, 5, 2, 1, 4);
            List<string> expected = new List<string>
            {
                "7 0",
                "4 0",
                "5 0",
                "2 0",
                "3 0",
                "6 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_FirstIsInDifferentColumn()
        {
            Table table = new Table(0, 0, 2, 3, 4, 5, 6);
            List<string> expected = new List<string>
            {
                "7 0",
                "6 0",
                "5 0",
                "4 0",
                "3 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_MiddleIsInSeparateColumn()
        {
            Table table = new Table(0, 1, 2, 3, 0, 4, 5);
            List<string> expected = new List<string>
            {
                "7 0",
                "6 0",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_LastIsInSeparateColumn()
        {
            Table table = new Table(0, 1, 2, 3, 4, 5, 0);
            List<string> expected = new List<string>
            {
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_EvenAndOddColumns()
        {
            Table table = new Table(0, 0, 1, 2, 3, 4, 5);
            List<string> expected = new List<string>
            {
                "6 0",
                "4 0",
                "7 0",
                "5 0",
                "3 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_AllInSeparateColumns()
        {
            Table table = new Table(0, 0, 0, 0, 0, 0, 0);
            List<string> expected = new List<string>
            {
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            List<string> moves = table.Arrange();

            Assert.AreEqual(expected, moves);
        }
    }
}