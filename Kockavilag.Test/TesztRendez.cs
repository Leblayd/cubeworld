﻿using System;
using System.Collections.Generic;
using Cubeworld;
using Féléves;
using NUnit;
using NUnit.Framework;

namespace Kockavilag.Test
{
    [TestFixture]
    public class TestRendez
    {
        [Test]
        public void Feleves()
        {
            var asztal = new Asztal(2, 0, 0, 3);
            var vart = new string[]
            {
                "1 0",
                "2 1",
                "4 0",
                "3 2",
                "4 3"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(vart, moves);
        }

        [Test]
        public void Test_AlreadyCorrect()
        {
            var asztal = new Asztal(0, 1, 2, 3, 4, 5, 6);
            var expected = new string[0];

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_SingleCube()
        {
            var asztal = new Asztal(0);
            var expected = new string[0];

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_ReversedSingleColumn()
        {
            var asztal = new Asztal(2, 3, 4, 5, 6, 7, 0);
            var expected = new string[]
            {
                "1 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_ReversedSingleColumn_WithCorrectEnds()
        {
            var asztal = new Asztal(0, 3, 4, 5, 6, 1, 2);
            var expected = new string[]
            {
                "7 0",
                "2 0",
                "3 0",
                "4 0",
                "5 0",
                "6 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_JumbledSingleColumn()
        {
            var asztal = new Asztal(6, 3, 1, 5, 7, 0, 2);
            var expected = new string[]
            {
                "4 0",
                "5 0",
                "7 0",
                "2 0",
                "3 0",
                "1 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_JumbledSingleColumn_WithCorrectEnds()
        {
            var asztal = new Asztal(0, 3, 6, 5, 2, 1, 4);
            var expected = new string[]
            {
                "7 0",
                "4 0",
                "5 0",
                "2 0",
                "3 0",
                "6 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_FirstIsInDifferentColumn()
        {
            var asztal = new Asztal(0, 0, 2, 3, 4, 5, 6);
            var expected = new string[]
            {
                "7 0",
                "6 0",
                "5 0",
                "4 0",
                "3 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_MiddleIsInSeparateColumn()
        {
            var asztal = new Asztal(0, 1, 2, 3, 0, 4, 5);
            var expected = new string[]
            {
                "7 0",
                "6 0",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_LastIsInSeparateColumn()
        {
            var asztal = new Asztal(0, 1, 2, 3, 4, 5, 0);
            var expected = new string[]
            {
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_EvenAndOddColumns()
        {
            var asztal = new Asztal(0, 0, 1, 2, 3, 4, 5);
            var expected = new string[]
            {
                "6 0",
                "4 0",
                "7 0",
                "5 0",
                "3 0",
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }

        [Test]
        public void Test_AllInSeparateColumns()
        {
            var asztal = new Asztal(0, 0, 0, 0, 0, 0, 0);
            var expected = new string[]
            {
                "2 1",
                "3 2",
                "4 3",
                "5 4",
                "6 5",
                "7 6"
            };

            var moves = asztal.Rendez();

            Assert.AreEqual(expected, moves);
        }
    }
}