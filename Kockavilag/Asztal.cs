﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Féléves
{
    class Kocka {
        public int szam;

        public Kocka(int szam) {
            this.szam = szam;
        }
    }
    public class Asztal
    {
        Kocka[,] felszin;
        int kockakSzama;
        public Asztal(int[]forras)
        {
            felszin = new Kocka[forras.Length,forras.Length];
            kockakSzama = forras.Length;

            int megtalalva = 0;
            
            while (megtalalva < forras.Length)
            {
                int j = 0;
                for (int i = 0; i < forras.Length; i++)
                {
                    if (megtalalva >= forras.Length)
                        break;

                    if (forras[i] == 0)
                    {
                        if (felszin[j, 0] == null)
                        {
                            felszin[j++, 0] = new Kocka(i + 1);
                            megtalalva++;
                        }
                    }
                    else
                    {
                        for (int k = 0; k < felszin.GetLength(0); k++)
                        {
                            for (int l = 0; l < felszin.GetLength(1); l++)
                            {
                                //ha ezen a helyen van kocka és ez az, aminek a tetejére kell rakni a mostanit
                                if (felszin[k, l] != null && felszin[k, l].szam == forras[i])
                                {
                                    //foglalt-e már a teteje
                                    if (felszin[k, l + 1] == null)
                                    {
                                        //berakja az új kockát
                                        felszin[k, l + 1] = new Kocka(i + 1);
                                        megtalalva++;
                                    }
                                    else
                                    {
                                        //Nem tudja értelmezni
                                        Console.WriteLine("HIBÁS");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public string[] Rendez()
        {
            int utasitasSzam = 0;
            string[] utasitasok = new string[100 * kockakSzama];
            while (MennyiHelyes() < kockakSzama)
            {
                //menjünk végig a kockák számain, de csak attól kezdve ami nincs már jó helyen
                for (int szam = MennyiHelyes() + 1; szam <= kockakSzama; szam++)
                {
                    //keressük meg az oszlopot amiben az első kocka van
                    int egyesOszlopa = MelyikOszlop(1);

                    //keressük meg az oszlopot amiben a "szám" számú kocka van
                    int mostaniOszlopa = MelyikOszlop(szam);

                    //amíg az oszlopa tetején van kocka ami nem ő
                    while (LegfelsoKocka(mostaniOszlopa).szam != szam)
                    {
                        //el kell mozgatni azokat az asztalra
                        int legfelsoSzama = LegfelsoKocka(mostaniOszlopa).szam;
                        utasitasok[utasitasSzam++] = legfelsoSzama + " " + 0;
                        AsztalraRak(legfelsoSzama);
                        //ha a tetejéről az 1-est mozgattuk el, akkor keressük meg megint
                        if (szam == 1)
                        {
                            egyesOszlopa = MelyikOszlop(1);
                        }
                    }

                    //ha a kocka abban az oszlopban van, mint az 1-es számú kocka, de a megfelelő helyen nem ő van
                    Kocka helyesPozicio = felszin[mostaniOszlopa, szam - 1];
                    if (mostaniOszlopa.Equals(egyesOszlopa) && (helyesPozicio == null || helyesPozicio.szam != szam))
                    {
                        //akkor el kell mozgatni az asztalra
                        utasitasok[utasitasSzam++] = szam + " " + 0;
                        AsztalraRak(szam);
                        //és folytatni kell, mert a most iterált számú kocka elmozdult
                        continue;
                    }
                    
                    //ha az első oszlopnak minden eddigi eleme helyes, és a mostani kocka pont odaillik
                    int egyesLegfelsoSzama = LegfelsoKocka(egyesOszlopa).szam;
                    if (MennyiHelyes() == egyesLegfelsoSzama && egyesLegfelsoSzama + 1 == szam)
                    {
                        //tegyük rá az egyest tartalmazó oszlop tetejére
                        EgyeshezRak(szam);
                        utasitasok[utasitasSzam++] = szam + " " + egyesLegfelsoSzama;
                    }
                }
            }
            //utasítások lerövidítése, hogy ne legyen olyan nagy tömb
            var u = new string[utasitasSzam];
            for (int i = 0; i < utasitasSzam; i++)
            {
                u[i] = utasitasok[i];
            }
            return u;
        }

        private void EgyeshezRak(int szam)
        {
            int x = MelyikOszlop(szam);
            int y = MelyikHely(szam);
            int egyesOszlopa = MelyikOszlop(1);
            //megkerressük az első üres helyet
            for (int i = 0; i < felszin.GetLength(1); i++)
            {
                if (felszin[egyesOszlopa, i] == null)
                {
                    //rakjuk az első kockát az egyes oszlop tetejére
                    felszin[egyesOszlopa, i] = felszin[x, y];
                    felszin[x, y] = null;
                }
            }
        }

        private void AsztalraRak(int szam)
        {
            int x = MelyikOszlop(szam);
            int y = MelyikHely(szam);
            //keresünk egy üres helyet az asztalon
            for (int i = 0; i < felszin.GetLength(0); i++)
            {
                if (felszin[i, 0] == null)
                {
                    //rakjuk az első kockát egy üres helyre 
                    felszin[i, 0] = felszin[x, y];
                    felszin[x, y] = null;

                    break;
                }
            }
        }

        private Kocka LegfelsoKocka(int mostaniOszlopa)
        {
            for (int i = felszin.GetLength(1) - 1; i >= 0; i--)
            {
                if (felszin[mostaniOszlopa, i] != null)
                {
                    return felszin[mostaniOszlopa, i];
                }
            }
            return null;
        }

        private int MelyikOszlop(int szam)
        {
            for (int i = 0; i < felszin.GetLength(0); i++)
            {
                for (int j = 0; j < felszin.GetLength(1); j++)
                {
                    if (felszin[i, j] != null && felszin[i, j].szam == szam)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private int MelyikHely(int szam)
        {
            for (int i = 0; i < felszin.GetLength(0); i++)
            {
                for (int j = 0; j < felszin.GetLength(1); j++)
                {
                    if (felszin[i, j] != null && felszin[i, j].szam == szam)
                    {
                        return j;
                    }
                }
            }
            return -1;
        }

        private int MennyiHelyes()
        {
            int helyes = 0;
            int egyesOszlopa = MelyikOszlop(1);
            //megnézzük, hogy az oszlop hány kockája van jó helyen
            for (int i = 0; i < felszin.GetLength(1); i++)
            {
                if (felszin[egyesOszlopa, i] != null)
                {
                    if (felszin[egyesOszlopa, i].szam == i + 1)
                    {
                        helyes++;
                    }
                    else
                    {
                        return helyes;
                    }
                }
            }
            return helyes;
        }
    }
}
