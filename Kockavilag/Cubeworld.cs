﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubeworld
{
    public static class LinqExtension
    {
        public static List<Table.Cube> CorrectPart(this Stack<Table.Cube> stack)
        {
            int id = 1;
            return stack.Reverse().ToList().TakeWhile(x => x.Id == id++).ToList();
        }

        public static bool IsCorrect(this Stack<Table.Cube> stack)
        {
            int id = 1;
            return stack.Reverse().ToList().All(x => x.Id == id++);
        }
    }

    public class Table
    {
        public List<Cube> Cubes { get; set; }
        public List<Stack<Cube>> CubeStacks { get; set; }
        private readonly Arm _arm;

        public Table(params int[] positions)
        {
            _arm = new Arm(this);
            CubeStacks = new List<Stack<Cube>>();
            int id = 1;
            Cubes = new List<int>(positions).Select(pos => new Cube(id++, pos)).ToList();

            int i = 0;
            while (Cubes.Count != CubeStacks.Sum(s => s.Count))
            {
                Cube cube = Cubes[i++ % Cubes.Count];
                if (CubeStacks.Any(stack => stack.Contains(cube)))
                    continue;

                if (cube.Position == 0)
                {
                    Stack<Cube> column = new Stack<Cube>();
                    column.Push(cube);
                    CubeStacks.Add(column);
                }
                else
                {
                    foreach (Stack<Cube> column in CubeStacks)
                    {
                        if (column.Peek().Id == cube.Position)
                        {
                            column.Push(cube);
                            break;
                        }
                    }
                }
            }
        }

        public List<string> Arrange()
        {
            List<string> log = new List<string>();
            while (CubeStacks.Count > 1 || !CubeStacks[0].IsCorrect())
            {
                foreach (Cube cube in Cubes)
                {
                    // Look at the column that the first block is in
                    // Check how many blocks are already in place.
                    Stack<Cube> columnOfFirst = CubeStacks.Find(stack => stack.Contains(Cubes[0]));
                    List<Cube> correct = columnOfFirst.CorrectPart();

                    // skip the iteration if that cube is perfectly in place 
                    if (cube.Id <= correct?.Count) continue;

                    // find the column that contains the current cube
                    Stack<Cube> column = CubeStacks.Find(stack => stack.Contains(cube));

                    // if the cube is in the same stack as the first but at the wrong position, move it
                    if (column.Equals(columnOfFirst) && column.Count != column.Peek().Id)
                    {
                        log.Add(_arm.Move(column, new Stack<Cube>()));
                        continue;
                    }

                    // if the one on top isn't what we're looking for, move it to the table
                    while (column.Peek() != cube)
                    {
                        Stack<Cube> newColumn = new Stack<Cube>();
                        log.Add(_arm.Move(column, newColumn));
                        // If the one we just moved to the table is the first cube,
                        // then change our reference of its column to the newly created one
                    }

                    if (correct?.Count == columnOfFirst.Count && columnOfFirst != column && columnOfFirst.Count + 1 == cube.Id) {
                        correct.Add(column.Peek());
                        log.Add(_arm.Move(column, columnOfFirst));
                    }
                }
            }
            return log;
        }

        public class Cube
        {
            public int Id { get; }
            public int Position { get; set; }

            public Cube(int id, int position)
            {
                Id = id;
                Position = position;
            }

            public override string ToString()
            {
                return Id + "";
            }
        }

        public class Arm
        {
            public Table Table { get; }

            public Arm(Table table)
            {
                Table = table;
            }

            public string Move(Stack<Cube> fromStack, Stack<Cube> toStack)
            {
                Cube toMove = fromStack.Pop();
                Cube onTopOf = toStack.Count > 0 ? toStack.Peek() : null;
                toStack.Push(toMove);

                if (fromStack.Count == 0)
                    Table.CubeStacks.Remove(fromStack);
                if (onTopOf == null)
                {
                    Table.CubeStacks.Add(toStack);
                    return $"{toMove.Id} 0";
                }
                return $"{toMove.Id} {onTopOf.Id}";
            }
        }
    }

    
}
